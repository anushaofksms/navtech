import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorMsg:boolean;
  constructor(private router:Router){ 
    this.errorMsg=false;
  }

  ngOnInit() {
  }

  loginData(formValue)
  {
    this.errorMsg=true;
    if(!formValue.invalid){
      this.router.navigate(['/order']);
    }
  }
}
