import { Component, OnInit } from '@angular/core';
import { OrderService } from './../order.service';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orderList:any;
  orderId:any;
  orderEditList={
    "orderNumber":'',
    "orderDueDate":'',
    "customerBuyerName":'',
    "customerAddress":'',
    "customerPhone":'',
    "orderTotal":''
  }

  constructor(private orderService:OrderService) {
    this.orderList=[];
   }
  
  ngOnInit() {
    this.getOrderList();
  }
  
  getOrderList(){
      try {
       this.orderService.getOrderList()
         .subscribe(response => {
           this.orderList=response
         }, error => {
           console.log('==Get error Details==', error)
         })
     } catch (ex) {
       console.log(ex)
     }
  }
  
  addOrder(data)
  {
    let addData={
      orderNumber:data.value.orderNumber,
      orderDueDate:data.value.orderDueDate,
      customerBuyerName:data.value.customerBuyerName,
      customerAddress:data.value.customerAddress,
      customerPhone:data.value.customerPhone,
      orderTotal:data.value.orderTotal
    }
    this.orderList.push(addData)
  }

  editOrder(list){
     this.orderEditList.orderNumber=list.orderNumber,
     this.orderEditList.orderDueDate=list.orderDueDate,
     this.orderEditList.customerBuyerName=list.customerBuyerName,
     this.orderEditList.customerAddress=list.customerAddress,
     this.orderEditList.customerPhone=list.customerPhone,
     this.orderEditList.orderTotal=list.orderTotal
  }

  onEditOrder(list){
    for(var i=0;i<this.orderList.length;i++)
    {
       if(this.orderList[i].orderNumber == list.orderNumber)
       {
        this.orderList[i].orderDueDate=list.orderDueDate,
        this.orderList[i].customerBuyerName=list.customerBuyerName,
        this.orderList[i].customerAddress=list.customerAddress,
        this.orderList[i].customerPhone=list.customerPhone,
        this.orderList[i].orderTotal=list.orderTotal
       }
    }
  }

  deleteGetId(id){
    this.orderId=id;
  }
  
  deleteData(){
    this.orderList.splice(this.orderId,1);
  }
  
}
